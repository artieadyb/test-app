import React from 'react'
import Content from '../Components.js/Content'

const DetailUser = ({isEdit}) => {
  return (
    <div className='d-flex px-5 py-4 my-2'>
        {isEdit ? (
            <Content title="Update User" isEdit/>
        ):(
            <Content title="Detail User"/>
        )}
    </div>
  )
}

export default DetailUser