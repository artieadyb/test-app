import React from 'react'
import Content from '../Components.js/Content'

const AddUser = () => {
  return (
    <div className='d-flex px-5 py-4 my-2'>
        <Content title="Add User Form"/>
    </div>
  )
}

export default AddUser