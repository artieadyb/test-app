import { axiosPrivate } from "../api/axiosPrivate";


const userServices =()=>{
    
    const getUsers = async (page, size) => {
        try {
          const data = await axiosPrivate
            .get(`/user`)
            .then(({ data }) => {
              return data.data;
            });
    
          return data;
        } catch (err) {
          console.error(err.message);
        }
    };

    const addUser = async (bodyReq) => {
        try {
          const data = await axiosPrivate
            .post("/user", bodyReq)
            .then(({ data }) => {
                alert(data.message)
                return data.message;
            });
    
          return data;
        } catch (err) {
          console.error(err.message);
        }
    };

    const getUserById = async (id) => {
        try {
          const data = await axiosPrivate
            .get(`/user/${id}`,)
            .then(({ data }) => {
                return data.data[0];
            });
    
          return data;
        } catch (err) {
          console.error(err.message);
        }
    };

    const updateUser = async (bodyReq) => {
        try {
          const data = await axiosPrivate
            .put("/user", bodyReq)
            .then(({ data }) => {
                alert(data.message)
                return data.message;
            });
    
          return data;
        } catch (err) {
          console.error(err.message);
        }
    };

    const deleteUser = async (id) => {
        try {
          await axiosPrivate.delete(`user/${id}`).then(({ data }) => {
            // return data;
            alert(`${data.message} deleted data with id ${id}`)
            window.location.reload();
          });
        } catch (err) {
          console.error(err.message);
        }
      };

    return{
        getUsers,
        addUser,
        getUserById,
        updateUser,
        deleteUser,
    }
}

export default userServices;