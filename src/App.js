import './App.css';
import Content from './Components.js/Content';

function App() {
  return (
    <div className="App d-flex px-5 py-4 my-2">
      <Content title="User Table"/>
    </div>
  );
}

export default App;
