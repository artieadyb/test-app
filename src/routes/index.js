import { createBrowserRouter } from "react-router-dom";
import App from "../App";
import AddUser from "../Pages/AddUser";
import DetailUser from "../Pages/DetailUser"

const router = createBrowserRouter([
    {
        path:'/',
        element:<App/>,
    },
    {
        path:'/:id',
        element:<DetailUser/>,
    },
    {
        path:'/:id/edit',
        element:<DetailUser isEdit/>,
    },
    {
        path:'/add',
        element:<AddUser/>,
    },
]);

export default router;