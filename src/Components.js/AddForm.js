import React, {useState} from 'react'
import { useNavigate } from 'react-router-dom'
import userServices from '../services/userServices'

const AddForm = () => {
    const navigate = useNavigate()
    const {addUser}=userServices()
    const [ name, setName] = useState()
    const [ userName, setUserName] = useState()
    const [ password, setPassword] = useState()
    const [ active, setActive] = useState()

    const handleSubmit = () => {
        const formData = {
            "namalengkap":name,
            "username":userName,
            "password":password,
            "status":active,
        }
        addUser(formData).then(()=>navigate('/'))
    }

  return (
    <div className="container">
        <form>
        <div className="row">
            <div className="col justify-content-start">
                <div className="row mb-3 mx-0">
                    <div className='col-3 '>
                        Nama Lengkap <span className='text-danger'>*</span>
                    </div>
                    <div className='col-1 '>
                        :
                    </div>
                    <div className='col-8 px-0 '>
                        <input 
                            className='form-control' 
                            type="text" 
                            onChange={(event)=>setName(event.target.value)}
                        />
                    </div>
                </div>
                <div className="row mb-3 mx-0">
                    <div className='col-3 '>
                        User Name <span className='text-danger'>*</span>
                    </div>
                    <div className='col-1 '>
                        :
                    </div>
                    <div className='col-8 px-0 '>
                        <input 
                            className='form-control' 
                            type="text" 
                            onChange={(event)=>setUserName(event.target.value)}
                        />
                    </div>
                </div>
                <div className="row mb-3 mx-0">
                    <div className='col-3 '>
                        Password <span className='text-danger'>*</span>
                    </div>
                    <div className='col-1 '>
                        :
                    </div>
                    <div className='col-8 px-0 '>
                        <input 
                            className='form-control' 
                            type="password" 
                            onChange={(event)=>setPassword(event.target.value)}
                        />
                    </div>
                </div>
                <div className="row mb-3 mx-0">
                    <div className='col-3 '></div>
                        <div className='col-1 '></div>
                        <div className='col-8 d-flex px-0 '>
                        <div className='d-flex'>
                            <input 
                                className = "form-check-input me-2" 
                                type='checkbox'
                                onChange={(event)=>setActive(event.target.checked ? "Active":"Innactive")}
                            />
                            <label>Active</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <div className='d-flex justify-content-center mt-5'>
            <button className='btn btn-success text-light' onClick={()=>handleSubmit()}>Submit</button>
            <button className='btn btn-danger text-light mx-4' onClick={()=>navigate('/')}>Cancel</button>
        </div>
    </div>
  )
}

export default AddForm