import React, {useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom';
import { BsLockFill, BsPencilSquare } from "react-icons/bs";
import { FaRegEye } from "react-icons/fa";
import userServices from '../services/userServices';

const Table = () => {
  const {getUsers, deleteUser} = userServices()
  const pathName = window.location.pathname;
  const navigate = useNavigate()
  const [dataUser, setDataUser]=useState()


  const handleDelete = (id) => {
    deleteUser(id)
  }

  useEffect(()=>{
    getUsers().then((res)=>setDataUser(res))
  },[])

  return (
    <div className='border shadow'>
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>User Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
            </thead>
            <tbody>
              {dataUser?.map((data,index)=>(

              <tr key={index}>
                <td>{index+1}</td>
                <td>{data?.namalengkap}</td>
                <td>{data?.username}</td>
                <td>{data?.status}</td>
                <td>
                  <div className='d-flex gap-1'>
                    <button
                      className="text-light bg-primary d-flex align-items-center py-2 px-2  border-0 rounded-1"
                      data-placement="bottom"
                      data-toggle="tooltip"
                      title="detail"
                      type="button"
                      tabIndex="0"
                      onClick={()=>navigate(`${pathName}${data?.userid}`)}
                    >
                      <FaRegEye/>
                    </button>
                    <button
                      className="text-light bg-success d-flex align-items-center py-2 px-2  border-0 rounded-1"
                      data-placement="bottom"
                      data-toggle="tooltip"
                      title="edit"
                      type="button"
                      tabIndex="0"
                      onClick={()=>navigate(`${pathName}${data?.userid}/edit`)}
                    >
                      <BsPencilSquare/>
                    </button>
                    <button
                      className={`text-light d-flex align-items-center py-2 px-2  border-0 rounded-1 bg-danger`}
                      data-placement="bottom"
                      data-toggle="tooltip"
                      title="delete"
                      type="button"
                      tabIndex="0"
                      onClick={()=>handleDelete(data?.userid)}
                    >
                      <BsLockFill/>
                    </button>
                  </div>
                </td>
              </tr>
              ))}
            </tbody>
        </table>
    </div>
  )
}
export default Table