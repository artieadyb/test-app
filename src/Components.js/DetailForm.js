import React, {useState, useEffect} from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import userServices from '../services/userServices'

const DetailForm = ({isEdit}) => {
    const navigate = useNavigate()
    const pathName = window.location.pathname;
    const { id } = useParams();
    const {getUserById, updateUser}=userServices()
    const [dataUser, setDataUser]=useState()
    const [ name, setName] = useState()
    const [ userName, setUserName] = useState()
    const [ password, setPassword] = useState()
    const [ active, setActive] = useState()

    const initValue ={
        name :dataUser?.namalengkap || "",
        userName :dataUser?.username || "",
        password :dataUser?.password || "",
        status: dataUser?.status || ""
    }

    const handleSubmit = () => {
        const formData = {
            "userid":id,
            "namalengkap":name ||initValue?.name,
            "username":userName ||initValue?.userName,
            "password":password  ||initValue?.password,
            "status":active  ||initValue?.status,
        }
        updateUser(formData).then(()=>navigate(`/${id}`))
    }

    useEffect(()=>{
        getUserById(id).then((res)=>setDataUser(res))
      },[])

  return (
    <div className="container">
        <form>
        <div className="row">
            <div className="col justify-content-start">
                <div className="row mb-3 mx-0">
                    <div className='col-3 '>
                        Nama Lengkap <span className='text-danger'>*</span>
                    </div>
                    <div className='col-1 '>
                        :
                    </div>
                    <div className='col-8 px-0 '>
                        <input 
                            defaultValue={initValue?.name}
                            className='form-control' 
                            type="text" 
                            disabled={!isEdit}
                            onChange={(event)=>setName(event.target.value)}
                        />
                    </div>
                </div>
                <div className="row mb-3 mx-0">
                    <div className='col-3 '>
                        User Name <span className='text-danger'>*</span>
                    </div>
                    <div className='col-1 '>
                        :
                    </div>
                    <div className='col-8 px-0 '>
                        <input 
                            defaultValue={initValue?.userName}
                            className='form-control' 
                            type="text" 
                            disabled={!isEdit}
                            onChange={(event)=>setUserName(event.target.value)}
                        />
                    </div>
                </div>
                {isEdit ? (
                    <div className="row mb-3 mx-0">
                        <div className='col-3 '>
                            Password <span className='text-danger'>*</span>
                        </div>
                        <div className='col-1 '>
                            :
                        </div>
                        <div className='col-8 px-0 '>
                            <input 
                                defaultValue={initValue?.password}
                                className='form-control' 
                                type="password" 
                                onChange={(event)=>setPassword(event.target.value)}
                            />
                        </div>
                    </div>
                ):(
                    <div></div>                
                )
                }
                <div className="row mb-3 mx-0">
                    <div className='col-3 '></div>
                        <div className='col-1 '></div>
                        <div className='col-8 d-flex px-0 '>
                        <div className='d-flex'>
                            <input 
                                checked={initValue?.status === 'Active' ? true : false}
                                className = "form-check-input me-2" 
                                type='checkbox'
                                disabled={!isEdit}
                                onChange={(event)=>setActive(event.target.checked ? "Active":"Innactive")}
                            />
                            <label>Active</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        {isEdit ?(
            <div className='d-flex justify-content-center mt-5'>
            <button className='btn btn-success text-light' onClick={()=>handleSubmit()}>Submit</button>
            <button className='btn btn-danger text-light mx-4' onClick={()=>navigate(`/${id}`)}>Cancel</button>
        </div>
        ):(
            <div className='d-flex justify-content-center mt-5'>
                <button className='btn btn-success text-light px-4' onClick={()=>navigate(`${pathName}/edit`)}>Edit</button>
                <button className='btn btn-danger text-light mx-4' onClick={()=>navigate('/')}>Cancel</button>
            </div>
        )}
    </div>
  )
}

export default DetailForm