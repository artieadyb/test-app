import React from 'react'
import { useNavigate } from 'react-router-dom';
import Table from './Table'
import AddForm from './AddForm';
import DetailForm from './DetailForm';

const Content = ({title, isEdit,...props}) => {

  const pathName = window.location.pathname;
  const navigate = useNavigate()

  return (
    <div className="content-wrapper w-100 bg-light rounded">
      <div
        className="content-header d-flex align-items-center px-4 bg-secondary rounded-top"
        style={{ background: "linear-gradient(to left,#006677, #4694a6)", height: "70px" }}
      >
        <div className="title text-light fw-semibold">{title}</div>
        <div className="ms-auto">
          <button
            type="button"
            className="add-button btn d-flex align-items-center btn-success text-light py-0 px-2"
            onClick={()=>navigate(`${pathName}add`)}
          >
            <span className="button-text fw-bold fs-4 mb-1 me-1">+</span> Add
            New
          </button>
        </div>
      </div>
      <div className="mx-4 py-4 ">
        {title === "User Table" &&
          <Table />
        }
        {title === "Add User Form" &&
          <AddForm />
        }
        {title === "Detail User" &&
          <DetailForm/>
        }
        {title === "Update User" &&
          <DetailForm isEdit/>
        }
      </div>
    </div>
  )
}

export default Content